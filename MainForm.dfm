object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu, biMinimize]
  Caption = 'ArvBot'
  ClientHeight = 358
  ClientWidth = 461
  Color = clBtnFace
  Constraints.MaxHeight = 385
  Constraints.MinHeight = 385
  Constraints.MinWidth = 469
  DoubleBuffered = True
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelPolls: TPanel
    Left = 217
    Top = 59
    Width = 244
    Height = 299
    Align = alClient
    TabOrder = 1
    object PanelQuestion: TPanel
      Left = 1
      Top = 1
      Width = 242
      Height = 88
      Align = alTop
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      DesignSize = (
        242
        88)
      object ButtonOpenPoll: TButton
        Left = 3
        Top = 5
        Width = 53
        Height = 25
        Hint = 'Until opened - votes will not be accepted'
        Caption = 'Open Poll'
        TabOrder = 0
        OnClick = ButtonOpenPollClick
      end
      object ButtonClosepoll: TButton
        Left = 3
        Top = 34
        Width = 53
        Height = 25
        Hint = 
          'Summarize results and stop accepting votes. Pressing with alread' +
          'y closed poll will repaste results.'
        Caption = 'Close Poll'
        TabOrder = 1
        OnClick = ButtonClosepollClick
        OnMouseDown = ButtonClosepollMouseDown
      end
      object EditQuestion: TEdit
        Left = 3
        Top = 63
        Width = 233
        Height = 21
        Hint = 'Poll'#39's question'
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 8
        TextHint = 'Poll'#39's question'
      end
      object ButtonLastcall: TButton
        Left = 116
        Top = 5
        Width = 60
        Height = 25
        Caption = 'Last Call'
        TabOrder = 4
        OnClick = ButtonLastcallClick
      end
      object ButtonClearVotes: TButton
        Left = 116
        Top = 34
        Width = 60
        Height = 25
        Hint = 'Will only clear votes below, not stop poll'
        Caption = 'Clear Votes'
        TabOrder = 5
        OnClick = ButtonClearVotesClick
      end
      object ButtonSaveQuestion: TButton
        Left = 178
        Top = 5
        Width = 57
        Height = 25
        Hint = 'Saves current question'
        Caption = 'Save Q'
        TabOrder = 6
        OnClick = ButtonSaveQuestionClick
      end
      object ButtonAddCase: TButton
        Left = 58
        Top = 5
        Width = 56
        Height = 25
        Caption = 'Add Case'
        TabOrder = 2
        OnClick = ButtonAddCaseClick
      end
      object ButtonRemoveCase: TButton
        Left = 58
        Top = 34
        Width = 56
        Height = 25
        Caption = 'Rem Case'
        TabOrder = 3
        OnClick = ButtonRemoveCaseClick
      end
      object ButtonRestoreQuestion: TButton
        Left = 178
        Top = 34
        Width = 57
        Height = 25
        Hint = 'Restores last question'
        Caption = 'Restore Q'
        TabOrder = 7
        OnClick = ButtonRestoreQuestionClick
      end
    end
    object PanelCase1: TPanel
      Left = 1
      Top = 89
      Width = 242
      Height = 50
      Align = alTop
      TabOrder = 1
      OnClick = PanelCase1Click
      DesignSize = (
        242
        50)
      object SpinEdit1: TSpinEdit
        Left = 192
        Top = 3
        Width = 47
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        MaxValue = 0
        MinValue = 0
        TabOrder = 1
        Value = 0
        OnChange = SpinEdit1Change
      end
      object ProgressBar1: TProgressBar
        Left = 5
        Top = 31
        Width = 234
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        Max = 1
        TabOrder = 2
      end
      object Edit1: TEdit
        Left = 5
        Top = 4
        Width = 185
        Height = 21
        Hint = 
          'Please remember, that first 2 symbols of any case should not be ' +
          'numbers!'
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 0
        TextHint = 'Case 1'
        OnExit = Edit1Exit
      end
    end
    object PanelCase2: TPanel
      Left = 1
      Top = 139
      Width = 242
      Height = 50
      Align = alTop
      TabOrder = 2
      DesignSize = (
        242
        50)
      object SpinEdit2: TSpinEdit
        Left = 192
        Top = 3
        Width = 47
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
        OnChange = SpinEdit1Change
      end
      object ProgressBar2: TProgressBar
        Left = 5
        Top = 31
        Width = 234
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        Max = 1
        TabOrder = 1
      end
      object Edit2: TEdit
        Left = 5
        Top = 3
        Width = 185
        Height = 21
        Hint = 
          'Please remember, that first 2 symbols of any case should not be ' +
          'numbers!'
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        TextHint = 'Case 2'
        OnExit = Edit1Exit
      end
    end
    object PanelCase3: TPanel
      Left = 1
      Top = 189
      Width = 242
      Height = 50
      Align = alTop
      TabOrder = 3
      DesignSize = (
        242
        50)
      object SpinEdit3: TSpinEdit
        Left = 192
        Top = 3
        Width = 47
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
        OnChange = SpinEdit1Change
      end
      object ProgressBar3: TProgressBar
        Left = 5
        Top = 31
        Width = 234
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        Max = 1
        TabOrder = 1
      end
      object Edit3: TEdit
        Left = 5
        Top = 3
        Width = 185
        Height = 21
        Hint = 
          'Please remember, that first 2 symbols of any case should not be ' +
          'numbers!'
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        TextHint = 'Case 3'
        OnExit = Edit1Exit
      end
    end
    object PanelCase4: TPanel
      Left = 1
      Top = 239
      Width = 242
      Height = 50
      Align = alTop
      TabOrder = 4
      DesignSize = (
        242
        50)
      object SpinEdit4: TSpinEdit
        Left = 192
        Top = 2
        Width = 47
        Height = 22
        TabStop = False
        Anchors = [akTop, akRight]
        MaxValue = 0
        MinValue = 0
        TabOrder = 0
        Value = 0
        OnChange = SpinEdit1Change
      end
      object ProgressBar4: TProgressBar
        Left = 5
        Top = 31
        Width = 234
        Height = 16
        Anchors = [akLeft, akTop, akRight]
        Max = 1
        Step = 1
        TabOrder = 1
      end
      object Edit4: TEdit
        Left = 5
        Top = 2
        Width = 185
        Height = 21
        Hint = 
          'Please remember, that first 2 symbols of any case should not be ' +
          'numbers!'
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 2
        TextHint = 'Case 4'
        OnExit = Edit1Exit
      end
    end
  end
  object PanelSettings: TPanel
    Left = 0
    Top = 59
    Width = 217
    Height = 299
    Align = alLeft
    Caption = 'PanelSettings'
    TabOrder = 2
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 215
      Height = 297
      ActivePage = TabSheetSettings
      Align = alClient
      TabOrder = 0
      object TabSheetMain: TTabSheet
        Caption = 'Main'
        object RichEdit1: TRichEdit
          Left = 0
          Top = 29
          Width = 207
          Height = 240
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clHighlightText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          HideSelection = False
          HideScrollBars = False
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
          Zoom = 100
        end
        object PanelPageControlMainTop: TPanel
          Left = 0
          Top = 0
          Width = 207
          Height = 29
          Align = alTop
          TabOrder = 1
          object ButtonClearMemo: TButton
            Left = 130
            Top = 2
            Width = 75
            Height = 25
            Caption = 'Clear Memo'
            TabOrder = 0
            OnClick = ButtonClearMemoClick
          end
        end
      end
      object TabSheetDebug: TTabSheet
        Caption = 'Debug'
        ImageIndex = 1
        object RichEditDebug: TRichEdit
          Left = 0
          Top = 46
          Width = 207
          Height = 223
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clHighlightText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssVertical
          TabOrder = 0
          Zoom = 100
        end
        object EditRaw: TEdit
          Left = 0
          Top = 25
          Width = 207
          Height = 21
          Hint = 
            'RAW messages to server. Use only if understand and for debugging' +
            '!'
          Align = alTop
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          TextHint = 'For RAW messages to server'
          OnKeyPress = EditRawKeyPress
        end
        object ButtonDumpLog: TButton
          Left = 0
          Top = 0
          Width = 207
          Height = 25
          Align = alTop
          Caption = 'Dump logfile'
          TabOrder = 2
          OnClick = ButtonDumpLogClick
        end
      end
      object TabSheetListvoted: TTabSheet
        Caption = 'List voted'
        ImageIndex = 3
        object ListBoxVoted: TListBox
          Left = 0
          Top = 0
          Width = 177
          Height = 269
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
        object ListBoxVotedOption: TListBox
          Left = 177
          Top = 0
          Width = 30
          Height = 269
          Align = alRight
          ItemHeight = 13
          TabOrder = 1
        end
      end
      object TabSheetSettings: TTabSheet
        Caption = 'Settings'
        ImageIndex = 2
        OnMouseDown = TabSheetSettingsMouseDown
        object LabelControlChar: TLabel
          Left = 3
          Top = 6
          Width = 100
          Height = 13
          Caption = 'Controlling character'
        end
        object CheckBoxFloodless: TCheckBox
          Left = 3
          Top = 25
          Width = 138
          Height = 17
          Hint = 'Bigger delay between bot'#39's pending messages'
          Caption = 'Flood-less (somewhat)'
          Checked = True
          ParentShowHint = False
          ShowHint = True
          State = cbChecked
          TabOrder = 0
          OnClick = CheckBoxFloodlessClick
        end
        object EditControllingChar: TEdit
          Left = 182
          Top = 3
          Width = 24
          Height = 21
          MaxLength = 1
          TabOrder = 1
          Text = '$'
          OnKeyPress = EditControllingCharKeyPress
        end
        object ButtonDebug: TButton
          Left = 0
          Top = 165
          Width = 75
          Height = 25
          Caption = 'Debug'
          TabOrder = 2
          OnClick = ButtonDebugClick
        end
        object CheckBoxDouble_Buffered: TCheckBox
          Left = 3
          Top = 48
          Width = 126
          Height = 17
          Caption = 'Double-buffered'
          Checked = True
          State = cbChecked
          TabOrder = 3
          OnClick = CheckBoxDouble_BufferedClick
        end
        object CheckBoxClearpollotions: TCheckBox
          Left = 3
          Top = 71
          Width = 166
          Height = 17
          Caption = 'Clear poll upon closing'
          TabOrder = 4
        end
        object SpinEditTimedpoll: TSpinEdit
          Left = 120
          Top = 92
          Width = 84
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 5
          Value = 120
        end
        object CheckBoxTimedpoll: TCheckBox
          Left = 3
          Top = 94
          Width = 111
          Height = 17
          Caption = 'Time-limited poll'
          TabOrder = 6
        end
      end
    end
  end
  object PanelStart: TPanel
    Left = 0
    Top = 0
    Width = 461
    Height = 59
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 390
      Top = 31
      Width = 64
      Height = 13
      Caption = 'Disconnected'
    end
    object LabelOverallvotes: TLabel
      Left = 390
      Top = 43
      Width = 64
      Height = 13
      Caption = 'Overall votes'
    end
    object ButtonStartbot: TButton
      Left = 386
      Top = 4
      Width = 68
      Height = 25
      Caption = 'Start ArvBot'
      TabOrder = 3
      OnClick = ButtonStartbotClick
    end
    object EditChannel: TEdit
      Left = 5
      Top = 6
      Width = 276
      Height = 21
      Hint = 'Channel name'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      TextHint = 'Twitch nickname/channel to connect to'
      OnExit = EditChannelExit
      OnKeyDown = EditChannelKeyDown
      OnKeyPress = EditChannelKeyPress
    end
    object ButtonHelp: TButton
      Left = 352
      Top = 31
      Width = 32
      Height = 25
      Caption = 'Help'
      PopupMenu = PopupMenuHelp
      TabOrder = 5
      OnClick = ButtonHelpClick
    end
    object EditOAuth: TEdit
      Left = 5
      Top = 33
      Width = 276
      Height = 21
      Hint = 'Put your OAuth here. More info in help'
      ParentShowHint = False
      PasswordChar = '*'
      ShowHint = True
      TabOrder = 1
      TextHint = 'Your OAuth, see help for more info'
      OnKeyPress = EditOAuthKeyPress
    end
    object ButtonSave: TButton
      Left = 284
      Top = 4
      Width = 100
      Height = 25
      Caption = 'Save OAuth to disk'
      TabOrder = 4
      OnClick = ButtonSaveClick
    end
    object ButtonLoad: TButton
      Left = 284
      Top = 31
      Width = 66
      Height = 25
      Caption = 'Load OAuth'
      TabOrder = 2
      OnClick = ButtonLoadClick
    end
  end
  object IdIRC1: TIdIRC
    ConnectTimeout = 0
    Host = 'irc.twitch.tv'
    IPVersion = Id_IPv4
    ReadTimeout = -1
    CommandHandlers = <>
    ExceptionReply.Code = '500'
    ExceptionReply.Text.Strings = (
      'Unknown Internal Error')
    Nickname = 'arvbot'
    Username = 'arvbot'
    Password = 'oauth:br9b5qznba9sye8jj6egulpnt75zzq'
    UserMode = []
    OnRaw = IdIRC1Raw
    Left = 56
    Top = 208
  end
  object Timer1: TTimer
    Interval = 250
    OnTimer = Timer1Timer
    Left = 120
    Top = 224
  end
  object TimerMessages: TTimer
    Interval = 3000
    OnTimer = TimerMessagesTimer
    Left = 168
    Top = 224
  end
  object IdAntiFreeze1: TIdAntiFreeze
    Left = 8
    Top = 208
  end
  object PopupMenuHelp: TPopupMenu
    MenuAnimation = [maTopToBottom]
    Left = 168
    Top = 176
    object MenuItemStarting: TMenuItem
      Caption = 'Starting with bot'
      OnClick = MenuItemStartingClick
    end
    object MenuItemOAuth: TMenuItem
      Caption = 'OAuth'
      OnClick = MenuItemOAuthClick
    end
    object MenuItemTips: TMenuItem
      Caption = 'Tips and Hints'
      OnClick = MenuItemTipsClick
    end
    object MenuItemReports: TMenuItem
      Caption = 'Bugs and Errors'
      OnClick = MenuItemReportsClick
    end
    object MenuItemContacts: TMenuItem
      Caption = 'Contacts'
      OnClick = MenuItemContactsClick
    end
  end
  object TimerTimedpollLC: TTimer
    Enabled = False
    OnTimer = TimerTimedpollLCTimer
    Left = 122
    Top = 267
  end
  object TimerTimedpoll: TTimer
    Enabled = False
    OnTimer = TimerTimedpollTimer
    Left = 80
    Top = 224
  end
  object IdIRCTest: TIdIRC
    ConnectTimeout = 0
    Host = 'irc.twitch.tv'
    IPVersion = Id_IPv4
    ReadTimeout = -1
    CommandHandlers = <>
    ExceptionReply.Code = '500'
    ExceptionReply.Text.Strings = (
      'Unknown Internal Error')
    Username = ''
    Password = ''
    UserMode = []
    OnRaw = IdIRCTestRaw
    Left = 128
    Top = 176
  end
  object TimerNoop: TTimer
    Interval = 300000
    OnTimer = TimerNoopTimer
    Left = 170
    Top = 265
  end
  object Timer2: TTimer
    Interval = 3000
    OnTimer = Timer2Timer
    Left = 80
    Top = 270
  end
end
